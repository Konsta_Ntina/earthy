## GROUP 04 - SANDCASTLΕ


>**A0_Demographics**

* Demographic Information

* Initial map sketches


>**A1_Configuring**

* Process

* Urban configuration


>**A2_Forming**

* Dynamic relaxation
  * Relaxation script
  * Relaxation script jpeg
  * Relaxation text

 

* Meshing by Python
  * Creating Coordinated in GH
  * ExcelPythonGrids
  * FloorSurfaceforPython
  * Flowchart by Python
  * Meshing by Python
  * PoR for Python

>**A3_Structuring**


* Bricklaying Vault
  * Bricklaying Vault
  * Bricklaying Vault

 
* Karamba3D
  * Karamba3D upper vaults
  * Karamba3D Vault and Dome structures_ceilingfloor1
  * Karamba3D Vaults testing speckle
 

* Bricklaying straight wall

* Muqarnas Python Bricklaying Daniella Group04

* TopologicalVoxelation PZN C Script Daniella
 

>**Final Deliverables**

* Physical models

* Renders

* Final presentation

* Final report

* Gif group 4

* Poster group 4

---

**Group 04**

Konstantina Chouliara 4744292 | Maria Dimas 4893344 | Daniella Naous 4290038 | Bart van Nimwegen 4484770 | Ronald Rijsterborgh 4483014 | Steven Engels 4813022

**Instructors**

Prof. Dr. Ir. Sevil Sariyildiz|
Dr. Ir. Fred Veer|
Dr. Ir. Pirouz Nourian|
Ir. Hans Hoogenboom |
Ir. Dirk Rinze Visser|
Ir. Shervin Azadi|
Ir. Frank Schnater
